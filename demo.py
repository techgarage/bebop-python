#!/usr/bin/python
"""
  ARDrone3 demo with autonomous navigation to two color Parrot Cap
  usage:
       ./demo.py <task> [<metalog> [<F>]]
"""
import sys
#import cv2

from bebop import Bebop
from commands import movePCMDCmd
#from capdet import detectTwoColors, loadColors

# this will be in new separate repository as common library fo robotika Python-powered robots
from apyros.metalog import MetaLog, disableAsserts
from apyros.manual import myKbhit, ManualControlException



def demo( drone ):
    print "Follow 2-color cap ..."
#  drone.videoCbk = videoCallback
#  drone.videoEnable()
    try:
        drone.trim()
        drone.takeoff()
        drone.wait(5.0)
       # drone.flyToAltitude( 1.5 )
       # for i in xrange(1000):
       #     print i,
       #     drone.update( cmd=None )
        drone.land()
    except ManualControlException, e:
        print
        print "ManualControlException"
        if drone.flyingState is None or drone.flyingState == 1: # taking off
            drone.emergency()
        drone.land()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print __doc__
        sys.exit(2)
    metalog=None
    if len(sys.argv) > 2:
        metalog = MetaLog( filename=sys.argv[2] )
    if len(sys.argv) > 3 and sys.argv[3] == 'F':
        disableAsserts()

    drone = Bebop( metalog=metalog )
    demo( drone )
    print "Battery:", drone.battery

# vim: expandtab sw=4 ts=4 

